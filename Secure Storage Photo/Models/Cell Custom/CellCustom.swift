//
//  CellCustom.swift
//  Secure Storage Photo
//
//  Created by piro on 21.01.21.
//

import UIKit

class CellCustom: UITableViewCell {
    
    static let reuseIdentifier = "Cell"

    @IBOutlet var customBacgroundView: UIView!
    @IBOutlet var customImage: UIImageView!
    @IBOutlet var customLabel: UILabel!
    
    func setSetting() {
        customBacgroundView.layer.cornerRadius = 12
        
        customBacgroundView.layer.masksToBounds = true
        
    }
    
    func setSelected(isSelected: Bool) {
        if isSelected == true {
            customImage.alpha = 0.5
        } else {
            customImage.alpha = 1.0
        }
    }
    
}

extension UIView {
func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
    let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
    let mask = CAShapeLayer()
    mask.path = path.cgPath
    self.layer.mask = mask
   }
}
