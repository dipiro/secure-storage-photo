//
//  CellCustomCollectionViewCell.swift
//  Secure Storage Photo
//
//  Created by piro on 31.01.21.
//

import UIKit

class CellCustomCollectionViewCell: UICollectionViewCell {
    
    static let reuseIdentifier = "CollectionCell"

    @IBOutlet var customBackroundCVC: UIView!
    @IBOutlet var customImageCVC: UIImageView!
    @IBOutlet var customLabelCVC: UILabel!
    
    func setSelected(isSelected: Bool) {
        if isSelected == true {
            customImageCVC.alpha = 0.5
        } else {
            customImageCVC.alpha = 1.0
        }
    }
    
    func setSetting() {
        customBackroundCVC.layer.cornerRadius = 16
        customImageCVC.contentMode = .scaleToFill
        customBackroundCVC.layer.masksToBounds = true
    }
    
}
