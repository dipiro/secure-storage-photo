//
//  AlertCutomUIView.swift
//  Secure Storage Photo
//
//  Created by piro on 17.01.21.
//

import UIKit

class AlertCustom: UIView {

    @IBOutlet var customTextField: UITextField!
    @IBOutlet var customDoneButton: UIButton!
    @IBOutlet var typeOne: UIButton!
    @IBOutlet var typeTwo: UIButton!
    @IBOutlet var alertView: UIView!
    
    var onAction: ((String, String)->())?
    var someText: String?
    var somePhoto: String?
    

    func setSetting() {
        
        alertView.layer.cornerRadius = 8
        
        typeOne.setImage(UIImage(named: "folderDefault")?.withRenderingMode(.alwaysOriginal), for: .normal)
        typeTwo.setImage(UIImage(named: "folderSetting")?.withRenderingMode(.alwaysOriginal), for: .normal)
    }
    
//   static func createBackgroundAlert() {
//        let backgroundAlert = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
//        backgroundAlert.backgroundColor = .none
//        self.view.addSubview(backgroundAlert)
//        
//        let blurEffect = UIBlurEffect(style: .dark)
//        let visualEffectView = UIVisualEffectView(effect: blurEffect)
//        visualEffectView.frame = backgroundAlert.bounds
//        backgroundAlert.addSubview(visualEffectView)
//    
//        UIView.animate(withDuration: 0.3) {
//            self.view.layoutIfNeeded()
//            backgroundAlert.alpha = 1
//        }
//    }
    
    @IBAction func customOnTypeOne(_ sender: UIButton) {
        somePhoto = "folderDefault"

    }
    @IBAction func customOnTypeTwo(_ sender: UIButton) {
        somePhoto = "folderSetting"

    }
    @IBAction func addPhoto(_ sender: Any) {
    }
    
    @IBAction func customOnDoneButton(_ sender: UIButton) {
//        guard customTextField.text != "" else { return someText = "Folder" }
        if customTextField.text == "" {
            someText = "Folder"
        } else  {
            someText = customTextField.text
        }
        if let action = onAction {
            action(self.someText ?? "Folder", self.somePhoto ?? "folderDefault")
        }
    }
    
}
