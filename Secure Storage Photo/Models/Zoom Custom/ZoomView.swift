//
//  ZoomView.swift
//  Secure Storage Photo
//
//  Created by piro on 11.02.21.
//

import Foundation
import UIKit

class ZoomView: UIView, UIScrollViewDelegate {
    
    @IBOutlet var scrollV: UIScrollView!
    @IBOutlet var imageV: UIImageView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        scrollV.delegate = self
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageV
    }
    
}
