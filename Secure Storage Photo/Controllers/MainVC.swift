//
//  ViewController.swift
//  Secure Storage Photo
//
//  Created by piro on 16.01.21.
//

import UIKit
import UserNotifications
import KeychainSwift

enum DataType: Int {
    case folder = 0
    case image
}

enum DisplayMode: Int {
    case tableView = 0
    case collectionView = 1
}

struct ProfileData {
    var type: DataType
    var name: String?
}

class MainVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    var rawData: [ProfileData] = []
    var dataByType: [DataType: [ProfileData]] = [:]
    var directoryURL: URL?
    var selectedIndexes: [IndexPath] = []
    
    var editMode = false
    
    var barSelect: UIBarButtonItem = UIBarButtonItem()
    var barFolder: UIBarButtonItem = UIBarButtonItem()
    var barDelete: UIBarButtonItem = UIBarButtonItem()
    var barSort: UIBarButtonItem = UIBarButtonItem()
    
    let fileManager = FileManager.default
    
    let displayMode = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        barFolder = UIBarButtonItem(title: "+", style: .done, target: self, action: #selector(addFolderHandler(_:)))
        barSort = UIBarButtonItem(title: "sort", style: .done, target: self, action: #selector(displayModeHanlder(_:)))
        barSelect = UIBarButtonItem(title: "Select", style: .done, target: self, action: #selector(editModeHandler(_:)))
        barDelete = UIBarButtonItem(title: "Delete", style: .done, target: self, action: #selector(barDeleteHandler(_:)))
        
        navigationItem.rightBarButtonItems = [barFolder, barSort, barSelect]
        
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "CellCustom", bundle: nil), forCellReuseIdentifier: CellCustom.reuseIdentifier)
        collectionView.register(UINib(nibName: "CellCustomCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: CellCustomCollectionViewCell.reuseIdentifier)
        
        
        let mode = displayMode.integer(forKey: "displayMode")
        
        if mode == 0 {
            tableView.alpha = 0
        } else {
            tableView.alpha = 1
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDisplayHasChanged(notification:)), name: .displayStyleHasChanged, object: nil)
        
        let notificationCenter = UNUserNotificationCenter.current()
        
        notificationCenter.requestAuthorization(options: [.badge, .alert, .sound]) { (isAuthorized, error) in
            if isAuthorized {
                
                let content = UNMutableNotificationContent()
                content.body = "Test notification"
                content.title = "TEST"
                
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 60*60, repeats: false)
                
                let identifier = "notification"
                
                let request = UNNotificationRequest(identifier: identifier,
                                                    content: content,
                                                    trigger: trigger)
                
                notificationCenter.add(request) { (error) in
                    if let error = error {
                        print(error)
                    }
                }
            } else {
                if let error = error {
                    print(error)
                }
            }
        }

        if directoryURL == nil {
            directoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        }
        getFiles()
        updateData()
        
    }
    
    @objc func onDisplayHasChanged(notification: NSNotification) {
        guard let style = notification.userInfo?["style"] as? String else { return }
        if style == "collection" {
            tableView.alpha = 0
            displayMode.set("0", forKey: "displayMode")
            print(" режим установлен на \(style)")
        } else {
            tableView.alpha = 1
            displayMode.set("1", forKey: "displayMode")
            print(" режим установлен на \(style)")
        }
    }
    
    func getFiles() {
        
        rawData.removeAll()
        
        if let directoryURL = directoryURL {
            do {
                let allObjects = try fileManager.contentsOfDirectory(at: directoryURL, includingPropertiesForKeys: nil)
                
                for object in allObjects {
                    let objectName = object.lastPathComponent
                    
                    if objectName.contains(".png") {
                        rawData.append(ProfileData(type: .image, name: objectName))
                    } else {
                        rawData.append(ProfileData(type: .folder, name: objectName))
                    }
                }
            } catch {
                print("error")
            }
        }
    }
    
    func updateData() {
        dataByType[.folder] = rawData.filter({ $0.type == .folder})
        dataByType[.image] = rawData.filter({ $0.type == .image})
        tableView.reloadData()
        collectionView.reloadData()
    }
    
    func createFolder(nameOfFolder: String) {
        if let directoryUrl = directoryURL {
            do {
                let newFolderUrl = directoryUrl.appendingPathComponent(nameOfFolder)
                try fileManager.createDirectory(at: newFolderUrl, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print("error")
            }
        }
    }
    
    // MARK: - barDeleteHandler
    @objc func barDeleteHandler(_ gesture: UIGestureRecognizer) {
        for indexPath in selectedIndexes {
            if let type = DataType(rawValue: indexPath.section),
               let objectsForType = dataByType[type],
               let directoryURL = directoryURL,
               let indexOfObject = rawData.firstIndex(where: { $0.name == objectsForType[indexPath.row].name }) {
                rawData.remove(at: indexOfObject)
                do {
                    let name = directoryURL.appendingPathComponent(objectsForType[indexPath.row].name!)
                    try fileManager.removeItem(at: name)
                } catch {
                    print ("error")
                }
            }
            selectedIndexes = []
            updateData()
        }
        editMode = false
        barSelect.title = "Select"
        navigationItem.leftBarButtonItem = nil

    }
    
    // MARK: - HANDLER EDITMODE
    @objc func editModeHandler(_ gesture: UIGestureRecognizer) {
        if editMode == true {
            editMode.toggle()
            
            barSelect.title = "Select"
            navigationItem.leftBarButtonItem = nil
            selectedIndexes = []
            updateData()
        } else {
            editMode.toggle()
            
            barSelect.title = "Cancel"
            navigationItem.leftBarButtonItem = barDelete
            updateData()
        }
    }
    
    // MARK:  - HANDLER SWIPE COLLECTION AND TABLE
    @objc func displayModeHanlder(_ gesture: UIGestureRecognizer) {
        if tableView.alpha == 1 {
            NotificationCenter.default.post(Notification(name: .displayStyleHasChanged, object: nil, userInfo: ["style": "collection"]))
        } else {
            NotificationCenter.default.post(Notification(name: .displayStyleHasChanged, object: nil, userInfo: ["style": "table"]))
        }
    }
    
    // MARK: - HANDLER ADDING
    @objc func addFolderHandler(_ gesture: UIGestureRecognizer) {
        let alertSheet = UIAlertController(title: "Choose", message: nil, preferredStyle: .actionSheet)
        let folder = UIAlertAction(title: "Folder", style: .default) { (_) in
            let alertFolder = UIAlertController(title: "Create Folder", message: nil, preferredStyle: .alert)
            
            alertFolder.addTextField { (textField) in
                textField.placeholder = "Some Name"
            }
            
            let okAction = UIAlertAction(title: "Ok", style: .default) { [weak self] (_) in
                if let textField = alertFolder.textFields?[0].text {
                    self?.createFolder(nameOfFolder: textField)
                    self?.getFiles()
                    self?.updateData()
                }
                
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            alertFolder.addAction(okAction)
            alertFolder.addAction(cancelAction)
            self.present(alertFolder, animated: true)
            
        }
        let image = UIAlertAction(title: "Image", style: .default) { [weak self] (_) in
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            self?.present(imagePicker, animated: true, completion: nil)
        }
        alertSheet.addAction(folder)
        alertSheet.addAction(image)
        present(alertSheet, animated: true, completion: nil)
        
    }
    
}

// MARK: - Extension Picker Controller
extension MainVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let originalImage = info[.originalImage] as? UIImage,
           let directoryURL = directoryURL {
            do {
                let randomName = uint(arc4random_uniform(10000))
                let newImageUrl = directoryURL.appendingPathComponent("\(randomName).png")
                
                let pngImageData = originalImage.pngData()
                
                try pngImageData?.write(to: newImageUrl)
                
                print(try fileManager.contentsOfDirectory(at: directoryURL, includingPropertiesForKeys: nil))
                
                self.rawData.append(ProfileData(type: .image, name: "\(randomName).png"))
                
                DispatchQueue.main.async {
                    self.getFiles()
                    self.updateData()
                }
            } catch {
                print("error")
            }
        }

    }
}

extension Notification.Name {
    static let displayStyleHasChanged = Notification.Name("displayStyleHasChanged")
}



