//
//  SesondViewController.swift
//  Secure Storage Photo
//
//  Created by piro on 25.01.21.
//

import UIKit

class ImageVC: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var stackView: UIStackView!
    
    var arrayOfImageURLs: [URL] = []
    var indexOfImage: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        
        fillStackView()
    }
    
    override func viewDidLayoutSubviews() {
        scrollView.contentOffset = CGPoint(x: view.bounds.width * indexOfImage, y: 0)

    }
    
    func fillStackView() {
        for imageURL in arrayOfImageURLs {
            let image = UIImage(contentsOfFile: imageURL.path)
            if let customV = Bundle.main.loadNibNamed("ZoomView", owner: nil, options: nil)?.first as? ZoomView {
                customV.imageV.image = image
                stackView.addArrangedSubview(customV)
                NSLayoutConstraint.activate([
                    customV.heightAnchor.constraint(equalTo: scrollView.frameLayoutGuide.heightAnchor),
                    customV.widthAnchor.constraint(equalTo: scrollView.frameLayoutGuide.widthAnchor)
                ])
            }
        }
    }
}
