//
//  Extensions + UICollectionView.swift
//  Secure Storage Photo
//
//  Created by piro on 11.02.21.
//

import Foundation
import UIKit

extension MainVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return dataByType.keys.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let type = DataType(rawValue: section),
              let dataForType = dataByType[type] else {
            return 0
        }
        return dataForType.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let type = DataType(rawValue: indexPath.section) else {
            return CGSize(width: 50, height: 50)
        }
        
        switch type {
        case .image:
            return CGSize(width: 120, height: 120)
        case .folder:
            return CGSize(width: 100, height: 100)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let type = DataType(rawValue: indexPath.section),
              let dataForType = dataByType[type],
              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellCustomCollectionViewCell.reuseIdentifier, for: indexPath) as? CellCustomCollectionViewCell,
              let directoryURL = directoryURL else {
            return UICollectionViewCell()
        }
        
        let currentData = dataForType[indexPath.row]
        cell.setSetting()
        cell.alpha = 1
        
        switch currentData.type {
        case .image:
            if let imageName = currentData.name {
                cell.customLabelCVC.text = currentData.name
                cell.customImageCVC.alpha = 1
                
                do {
                    let imageUrl = directoryURL.appendingPathComponent(imageName)
                    cell.customImageCVC.image = UIImage(contentsOfFile: imageUrl.path)
                } catch {
                    print("Something went wrong")
                }
                
                if editMode == true  {
                    cell.setSelected(isSelected: selectedIndexes.contains(indexPath))
                    return cell
                }
                return cell
            }
        case .folder:
            cell.customImageCVC.alpha = 1
            cell.customLabelCVC.text = currentData.name
            cell.customImageCVC.image = UIImage(named: "folderDefault")
            
            if editMode == true  {
                cell.setSelected(isSelected: selectedIndexes.contains(indexPath))
            }
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let type = DataType(rawValue: indexPath.section),
              let dataForType = dataByType[type] else {
            return
        }
        

        
        let currentData = dataForType[indexPath.row]
        
        if editMode == true {
            if let index = selectedIndexes.firstIndex(of: indexPath) {
                selectedIndexes.remove(at: index)
                updateData()
            } else {
                selectedIndexes.append(indexPath)
                updateData()
            }
        } else {
            
            switch currentData.type {
            case .folder:
                if let folderVC = storyboard?.instantiateViewController(withIdentifier: "main") as? MainVC,
                   let folderName = currentData.name,
                   let directoryURL = directoryURL {
                    folderVC.directoryURL = directoryURL.appendingPathComponent(folderName)
                    
                    let mode = displayMode.integer(forKey: "displayMode")
                    navigationController?.pushViewController(folderVC, animated: true)
                }
            case .image:
                if let directoryURL = directoryURL,
                   let imageVC = storyboard?.instantiateViewController(withIdentifier: "secondVC") as? ImageVC {
                    
                    do {
                        let contentOfDirectory = try fileManager.contentsOfDirectory(at: directoryURL, includingPropertiesForKeys: nil)
                        var arrayOfImagesURLs: [URL] = []
                        for object in contentOfDirectory {
                            let objectName = object.lastPathComponent
                            if (objectName.contains(".png")) {
                                arrayOfImagesURLs.append(object)
                            }
                        }
                        
                        imageVC.arrayOfImageURLs = arrayOfImagesURLs
                    } catch {
                        print("error")
                    }
                    
                    imageVC.indexOfImage = CGFloat(indexPath.row)
                    present(imageVC, animated: true, completion: nil )
                }
            }

            collectionView.reloadData()
        }
    }
    
}
