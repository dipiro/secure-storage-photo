//
//  Extensions + UITableView.swift
//  
//
//  Created by piro on 11.02.21.
//

import Foundation
import UIKit

extension MainVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataByType.keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let type = DataType(rawValue: section),
              let dataForType = dataByType[type] else {
            return 0
        }
        return dataForType.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let type = DataType(rawValue: indexPath.section) else {
            return 40
        }
        switch type {
        case .image:
            return 150
        case .folder:
            return 100
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let type = DataType(rawValue: indexPath.section),
              let dataForType = dataByType[type],
              let cell = tableView.dequeueReusableCell(withIdentifier: CellCustom.reuseIdentifier, for: indexPath) as? CellCustom,
              let directoryURL = directoryURL else {
            return UITableViewCell()
        }
        cell.setSetting()
        
        let currentData = dataForType[indexPath.row]
        switch currentData.type {
        case .image:
            if let imageName = currentData.name {
                cell.customLabel.text = currentData.name
                cell.customImage.alpha = 1
                do {
                    let imageUrl = directoryURL.appendingPathComponent(imageName)
                    cell.customImage.image = UIImage(contentsOfFile: imageUrl.path)
                } catch {
                    print("Something went wrong")
                }
                
                if editMode == true  {
                    cell.setSelected(isSelected: selectedIndexes.contains(indexPath))
                    return cell
                }
                
                return cell
            }
        case .folder:
            cell.customLabel.text = currentData.name
            cell.customImage.image = UIImage(named: "folderDefault")
            cell.customImage.alpha = 1
            
            if editMode == true  {
                cell.setSelected(isSelected: selectedIndexes.contains(indexPath))
                return cell
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let type = DataType(rawValue: indexPath.section),
              let dataForType = dataByType[type] else {
            return
        }
        
        if editMode == true {
            if let index = selectedIndexes.firstIndex(of: indexPath) {
                selectedIndexes.remove(at: index)
                updateData()
            } else {
                selectedIndexes.append(indexPath)
                updateData()
            }
            
        } else {
            let currentData = dataForType[indexPath.row]
            switch currentData.type {
            case .folder:
                if let folderVC = storyboard?.instantiateViewController(withIdentifier: "main") as? MainVC,
                   let folderName = currentData.name,
                   let directoryURL = directoryURL {
                    folderVC.directoryURL = directoryURL.appendingPathComponent(folderName)
                    
                    let mode = displayMode.integer(forKey: "displayMode")
                    print(mode)
                    
//                    if mode == 1 {
//                        tableView.alpha = 1
//                    } else {
//                        tableView.alpha = 0
//                    }
                    navigationController?.pushViewController(folderVC, animated: true)
                }
                
            case .image:
                if let directoryURL = directoryURL,
                   let imageVC = storyboard?.instantiateViewController(withIdentifier: "secondVC") as? ImageVC {
                    do {
                        let contentOfDirectory = try fileManager.contentsOfDirectory(at: directoryURL, includingPropertiesForKeys: nil)
                        var arrayOfImagesURLs: [URL] = []
                        for object in contentOfDirectory {
                            let objectName = object.lastPathComponent
                            if (objectName.contains(".png")) {
                                arrayOfImagesURLs.append(object)
                            }
                        }
                        
                        imageVC.arrayOfImageURLs = arrayOfImagesURLs
                    } catch {
                        print("error")
                    }

                    imageVC.indexOfImage = CGFloat(indexPath.row)
                    present(imageVC, animated: true, completion: nil )
                }
            }
            tableView.reloadData()
        }
    }
}
