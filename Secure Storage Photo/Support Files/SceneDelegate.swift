//
//  SceneDelegate.swift
//  Secure Storage Photo
//
//  Created by piro on 16.01.21.
//

import UIKit
import KeychainSwift
import LocalAuthentication


class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    let keychain = KeychainSwift()
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let _ = (scene as? UIWindowScene) else { return }
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    //при разворачивания приложения
    func sceneWillEnterForeground(_ scene: UIScene) {
        
        let setedPasswordUD = keychain.get("password")
        
        let alertSheet = UIAlertController(title: "S", message: nil, preferredStyle: .actionSheet)
        let textPassword = UIAlertAction(title: "Password", style: .default) { (_) in
            if setedPasswordUD == nil {
                let setPasswordAlert = UIAlertController(title: "Set password", message: nil, preferredStyle: .alert)
                setPasswordAlert.addTextField { (textField) in
                    textField.placeholder = "Password"
                    textField.isSecureTextEntry = true
                }
                
                let okAction = UIAlertAction(title: "Ok", style: .default) { [weak self] (_) in
                    if let setPassword = setPasswordAlert.textFields?[0].text {
                        self?.keychain.set(setPassword, forKey: "password")
                    }
                }
                setPasswordAlert.addAction(okAction)
                self.window?.rootViewController?.present(setPasswordAlert, animated: true, completion: nil)
                
            } else {
                let getPasswordAlert = UIAlertController(title: "Enter password", message: nil, preferredStyle: .alert)
                getPasswordAlert.addTextField { (textField) in
                    textField.placeholder = "Password"
                    textField.isSecureTextEntry = true
                }
                
                let okAction = UIAlertAction(title: "Ok", style: .default) { [weak self] (_) in
                    if let getPassword = getPasswordAlert.textFields?[0].text {
                        if getPassword != setedPasswordUD {
                            self?.window?.rootViewController?.present(getPasswordAlert, animated: true, completion: nil)
                        }
                    }
                    
                }
                getPasswordAlert.addAction(okAction)

                self.window?.rootViewController?.present(getPasswordAlert, animated: true, completion: nil)
            }
        }
        let touchPassword = UIAlertAction(title: "Touch",style: .default){ (_) in
            let context = LAContext()
            
            if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) {
                        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Please perform biometric authentication") { (success, error) in
                            if success {
                                    print("success")
                            } else {
//                                let errorAlert = UIAlertController(title: "Authentication failed", message: nil, preferredStyle: .alert)
//                                errorAlert.addAction(UIAlertAction(title: "Ok", style: .default))
//                                self.window?.rootViewController?.present(alertSheet, animated: true)
                            }
                        }
                    }
            
        }
        
        alertSheet.addAction(textPassword)
        alertSheet.addAction(touchPassword)
        window?.rootViewController?.present(alertSheet, animated: true)
        
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    
}

